<?php
namespace Core\Repositories\Eloquent;

use Core\Repositories\Contracts\UserRepositoryContract;
use App\Models\User;

class UserRepository implements UserRepositoryContract
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function paginate($option)
    {
        return $this->user->paginate($option);
    }

    public function find($id)
    {
        return $this->user->findOrFail($id);
    }

    public function store(array $data)
    {
        return $this->user->create($data);
    }

    public function update($id,array $data)
    {
        return $this->user->findOrFind($id)->update($data);
    }

    public function destroy($id)
    {
        return $this->user->findOrFind($id)->destroy();
    }


}
