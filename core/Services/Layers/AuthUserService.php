<?php
namespace Core\Services\Layers;
use Illuminate\Support\Facades\Auth;

class  AuthUserService {

    public function __construct()
    {
//        auth()->shouldUse('api_user');
    }

    /**
     * @param array $data
     * @return bool
     */
    public function login(array $data)
    {
        return Auth::attempt($data);
    }

//    public function createTokenPasspost()
//    {
//        return auth()->user()->createToken('MyApp')->accessToken;
//    }


//    public function logout()
//    {
//        return auth()->logout();
//    }
}
