<?php
namespace Core\Providers;
use Core\Services\Layers\UserService;
use Core\Services\Contracts\UserServiceContract;
use Core\Repositories\Eloquent\UserRepository;
use Core\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryContract::class, UserRepository::class);
        $this->app->bind(UserServiceContract::class, UserService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

