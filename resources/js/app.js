require('./bootstrap');
import Vue from 'vue';
import AllStore from './store/AllStore';
import router from './router'
Vue.router = router;
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
import Ecomerce from './components/App'

Vue.config.productionTip = false;


const app = new Vue({
    el: '#app',
    router,
    store: AllStore,
    components: {
        Ecomerce
    }
});
