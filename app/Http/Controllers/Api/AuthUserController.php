<?php

namespace App\Http\Controllers\Api;
use Core\Services\Layers\AuthUserService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;

class AuthUserController extends Controller
{
    protected $authUserService;
    public function __construct(AuthUserService $authUserService)
    {
        $this->authUserService = $authUserService;
    }

    public function login(Request $request)
    {
//        try {
//
//        } catch (Exception $e) {
//            return response()->json(false);
//        }
//        $logined = $this->authUserService->login([
//            'user_email' => $request->user_email,
//            'user_password' => bcrypt($request->user_password),
//        ]);
        $data = [
            'user_email' => $request->user_email,
            'password' => bcrypt($request->user_password),
        ];
        print_r(Auth::attempt($data));
        if (Auth::attempt($data))
        {
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }
}
