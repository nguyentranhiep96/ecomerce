<?php

namespace App\Http\Controllers\Api;
use Core\Services\Contracts\UserServiceContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Exception;
use Log;

class UserController extends Controller
{
    protected $userService;
    public function __construct(UserServiceContract $userService)
    {
        $this->userService = $userService;
    }

    public function register(RegisterRequest $request)
    {
        try {
            $users= $this->userService->store([
                'user_name' => $request->user_name,
                'user_email' => $request->user_email,
                'password' => bcrypt($request->user_password),
                'user_status' => $request->user_status,
            ]);
            return response()->json($users);
        } catch (Exception $e) {
            Log::debug($e);
            return response()->json(false);
        }
    }
}
